import React, { Component } from 'react';

export default class TodoRowView extends Component {
  constructor(props) {
    super(props);

    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  handleEdit(event) {
    event.preventDefault();

    this.props.handleEdit();
  }

  handleDelete(event) {
    event.preventDefault();

    this.props.handleDelete();
  }

  render() {
    return (
      <tr>
        <td>{this.props.todo.id}</td>
        <td>{this.props.todo.title}</td>
        <td><a href="#" onClick={this.handleEdit} >Edit</a> <a href="#" onClick={this.handleDelete} >Remove</a></td>
      </tr> 
    );   
  }
}
