import React, { Component } from 'react';

import TodoRowView from './TodoRowView';
import TodoRowEdit from './TodoRowEdit';

export default class TodoRow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inEditMode: false
    };

    this.handleSave = this.handleSave.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleSave(newTitle) {
    this.props.handleSave(this.props.todo.id, newTitle);

    // по сути, ререндеринг происходит изза обновления состояния родителя -- TodoComponent,
    // но обновления состояния этого компонента не происходит, поэтому обновляем состояние ручками,
    // что триггерит второй ререндеринг этого компонента, связанный уже с обновлением состония самого компонента, а не его родителя
    // в целом и общем получаем два ререндеринга на одно обновление поля данных сущности..
    // это выглядит неоч, но Димас гоаорит вроде по другому никак.
    this.setState({
      inEditMode: false
    });
  }

  handleCancel() {
    this.setState({
      inEditMode: false
    });
  }

  handleEdit() {
    this.setState({
      inEditMode: true
    });
  }

  handleDelete() {
    this.props.handleDelete(this.props.todo.id);
  }

  render() {
    if (this.state.inEditMode) {
      return (
        <TodoRowEdit todo={this.props.todo} handleSave={this.handleSave} handleCancel={this.handleCancel} />
      );
    }

    return (
      <TodoRowView todo={this.props.todo} handleEdit={this.handleEdit} handleDelete={this.handleDelete} />
    );
  }
}
