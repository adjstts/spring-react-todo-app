import React, { Component } from 'react';

export default class TodoRowEdit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: this.props.todo.title
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  handleChange(event) {
    this.setState({
      title: event.target.value
    });
  }

  handleSave(event) {
    event.preventDefault();

    if (this.state.title !== this.props.todo.title) {
      this.props.handleSave(this.state.title);
    }
  }

  handleCancel(event) {
    event.preventDefault();

    this.props.handleCancel();
  }

  render() {
    return (
      <tr>
        <td>{this.props.todo.id}</td>
        <td><input type="text" value={this.state.title} onChange={this.handleChange} /></td>
        <td><a href="#" onClick={this.handleSave} >Save</a> <a href="#" onClick={this.handleCancel} >Cancel</a></td>
      </tr>
    );
  }
}
