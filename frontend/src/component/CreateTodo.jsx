import React, { Component } from 'react';

const DEFAULT_STATE = {
  title: ""
};

export default class CreateTodo extends Component {
  constructor(props) {
    super(props);

    this.state = DEFAULT_STATE;

    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleTitleChange(event) {
    this.setState({
      title: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.handleCreate({
      title: this.state.title
    });

    this.setState(DEFAULT_STATE);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" name="title" placeholder="todo title" value={this.state.title} onChange={this.handleTitleChange} />
        <input type="submit" value="Create" />
      </form>
    );
  }
}
