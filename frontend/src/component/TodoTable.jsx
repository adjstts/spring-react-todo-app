import React, { Component } from 'react';

import TodoRow from './todo-row/TodoRow';

export default class TodoTable extends Component {
  render() {
    return (
      <table>
        <thead>
          <tr><th>Id</th><th>Title</th><th>Actions</th></tr>
        </thead>
        <tbody>{this.props.todos.map(todo => <TodoRow todo={todo} handleSave={this.props.handleSave} handleDelete={this.props.handleDelete} />)}</tbody>
      </table>
    );
  }
}
