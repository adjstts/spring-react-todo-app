import React, { Component } from 'react';

import Constants from './constants';
import Rest from './rest';

import CreateTodo from './CreateTodo';
import TodoTable from './TodoTable';

export default class TodoComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: []
    };

    this.handleRead = this.handleRead.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.handleCreated = this.handleCreated.bind(this);
    this.handleSave = this.handleSave.bind(this);
    // this.handleSaved = this.handleSaved.bind(this); // не нужно, потому что вызывается из arrow-функции
    this.handleDelete = this.handleDelete.bind(this);
    // this.handleDeleted = this.handleDeleted.bind(this); // не нужно, потому что вызывается из arrow-функции
  }

  // https://reactjs.org/docs/state-and-lifecycle.html#adding-lifecycle-methods-to-a-class
  componentDidMount() {
    Rest.getJson(Constants.TODO_API_URL).then(this.handleRead);
  }

  handleRead(readData) {
    this.setState({
      todos: readData
    });
  }

  handleCreate(createData) {
    // TODO
    // 1. sync with server
    // 1.1 post create data to server
    // 1.2 receive createdData from server which includes newly created todo instance id.
    // 2. update state (push createdData to state.todos collection)
    Rest.postJson(Constants.TODO_API_URL, createData).then(this.handleCreated);
  }

  handleCreated(createdData) {
    this.setState((state, props) => ({
      todos: state.todos.concat(createdData)
    })); 
  }

  handleSave(saveId, saveTitle) {
    let saveData = {
      title: saveTitle
    };

    Rest.putJson(this.todoItemURL(saveId), saveData)
      .then(() => this.handleSaved(saveId, saveTitle));
  }

  handleSaved(savedId, savedTitle) {
    this.setState((state, props) => {
      let todos = state.todos;
      let savedIndex = this.findTodoIndexById(todos, savedId)

      todos[savedIndex].title = savedTitle;

      return {
        todos
      };
    });
    // setState всегда триггерит ререндеринг компонента, вызывая render для его и его детей,
    // но состоянием дочерних компонентов надо управлять руками.
  }

  handleDelete(deleteId) {
    Rest.deleteJson(this.todoItemURL(deleteId))
      .then(() => this.handleDeleted(deleteId));
  }

  handleDeleted(deletedId) {
    this.setState((state, props) => {
      let todos = state.todos;
      let deletedIndex = this.findTodoIndexById(todos, deletedId);

      todos.splice(deletedIndex, 1);

      return {
        todos
      };
    });
  }

  findTodoIndexById(todos, id) {
    return todos.findIndex(todo => todo.id === id);
  }

  todoItemURL(id) {
    return Constants.TODO_API_URL + "/" + id;
  }

  render() {
    return (
      <div>
        <CreateTodo handleCreate={this.handleCreate} />
        <TodoTable todos={this.state.todos} handleSave={this.handleSave} handleDelete={this.handleDelete} />
      </div>
    );
  }
}
