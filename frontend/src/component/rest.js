const HEADERS = {
  "Content-Type": "application/json"
};

function responseToJson(response) {
  return response.json();
}

var Rest = {

  postJson(url, data) {
    return fetch(url, {
      method: "POST",
      headers: HEADERS,
      body: JSON.stringify(data)
    })
      .then(responseToJson);
  },

  getJson(url) {
    return fetch(url, {
      method: "GET",
      headers: HEADERS
    })
      .then(responseToJson);
  },

  putJson(url, data) {
    return fetch(url, {
      method: "PUT",
      headers: HEADERS,
      body: JSON.stringify(data)
    });
  },

  deleteJson(url) {
    return fetch(url, {
      method: "DELETE",
      headers: HEADERS
    });
    // здесь, во-первых, ничего преобразовывать в json не нужно, потому что серверный метод возвращает ответ с пустым телом
    // во-вторых, так как сервер возвращает ответ с пустым телом, попытка вызвать метод .json завершается исключением.
  }

};

export default Rest;
