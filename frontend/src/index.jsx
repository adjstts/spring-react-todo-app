import React from 'react';
import ReactDOM from 'react-dom';

import TodoComponent from './component/TodoComponent';

ReactDOM.render(
  // можно было здесь пофетчить исходный список и пробросить
  // в состояние компонента через props, но благодаря
  // lifecycle методам, фетч происходит внутри самого компонента,
  // там же, где хранится состояние.
  <TodoComponent />,
  document.getElementById("root")
);
