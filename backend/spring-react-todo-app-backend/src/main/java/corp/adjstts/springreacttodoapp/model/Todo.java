package corp.adjstts.springreacttodoapp.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Todo {
    private static final String ID_GENERATOR_NAME = "todo-id-generator";

    // https://docs.jboss.org/hibernate/orm/5.2/userguide/html_single/Hibernate_User_Guide.html#identifiers-generators-sequence
    @Id
    @GeneratedValue(
        strategy = GenerationType.SEQUENCE,
        generator = ID_GENERATOR_NAME)
    @SequenceGenerator(
        name = ID_GENERATOR_NAME,
        sequenceName = "seq$todo$id",
        allocationSize = 1) // https://stackoverflow.com/questions/9861416/hibernate-generates-negative-id-values-when-using-a-sequence при этом в доке hibernate об этом ни слова
    private Long id;

    private String title;

//    Почитать, как это генерить
//    https://docs.jboss.org/hibernate/orm/5.2/userguide/html_single/Hibernate_User_Guide.html#mapping-generated
//    private LocalDateTime created;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    public LocalDateTime getCreated() {
//        return created;
//    }
//
//    public void setCreated(LocalDateTime created) {
//        this.created = created;
//    }
}
