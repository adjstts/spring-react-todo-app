package corp.adjstts.springreacttodoapp.service;

import corp.adjstts.springreacttodoapp.dao.TodoRepository;
import corp.adjstts.springreacttodoapp.model.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TodoService {
    private final TodoRepository repository;

    @Autowired
    public TodoService(TodoRepository repository) {
        this.repository = repository;
    }

    // тупо в getting-started туториалах сказано прикрутить эту аннотацию и всё
    // в доке spring boot ни слова об автоконфигурации перехватчика транзакционных методов.
    @Transactional
    public Todo create(Todo newTodo) {
        return repository.create(newTodo);
    }

    public List<Todo> read() {
        return repository.list();
    }

    @Transactional
    public void update(Long id, Todo newTodo) {
        Todo todo = repository.find(id);
        todo.setTitle(newTodo.getTitle());
    }

    @Transactional
    public void delete(Long id) {
        repository.remove(id);
    }
}
