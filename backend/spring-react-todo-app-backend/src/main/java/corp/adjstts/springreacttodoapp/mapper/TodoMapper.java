package corp.adjstts.springreacttodoapp.mapper;

import corp.adjstts.springreacttodoapp.dto.TodoCreateDTO;
import corp.adjstts.springreacttodoapp.model.Todo;
import org.springframework.stereotype.Component;

@Component
public class TodoMapper {
    public Todo mapFromCreateDTO(TodoCreateDTO dto) {
        Todo todo = new Todo();
        todo.setTitle(dto.getTitle());

        return todo;
    }
}
