package corp.adjstts.springreacttodoapp.controller;

import corp.adjstts.springreacttodoapp.dto.TodoCreateDTO;
import corp.adjstts.springreacttodoapp.mapper.TodoMapper;
import corp.adjstts.springreacttodoapp.model.Todo;
import corp.adjstts.springreacttodoapp.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/todo")
public class TodoController {
    private final TodoService service;
    private final TodoMapper mapper;

    @Autowired
    public TodoController(TodoService service, TodoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    Todo create(@RequestBody TodoCreateDTO dto) { // TODO notnull validation
        return service.create(mapper.mapFromCreateDTO(dto));
    }

    @GetMapping
    List<Todo> read() {
        return service.read();
    }

    @PutMapping("/{id}")
    void update(@PathVariable Long id, @RequestBody TodoCreateDTO dto) {
        service.update(id, mapper.mapFromCreateDTO(dto));
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
