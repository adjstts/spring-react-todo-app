package corp.adjstts.springreacttodoapp.dao;

import corp.adjstts.springreacttodoapp.model.Todo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class TodoRepository {

    // про автоконфигурацию PersistenceContext тоже ни слова в доке spring boot
    // написал тупо наугад
    @PersistenceContext
    private EntityManager entityManager;

    public Todo create(Todo todo) {
        entityManager.persist(todo);

        // нам нужен ид сохраненной сущности сразу, а не по окончании транзакции
        entityManager.flush();

        return todo;
    }

    public List<Todo> list() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Todo> query = builder.createQuery(Todo.class);
        Root<Todo> root = query.from(Todo.class);

        query.select(root);

        return entityManager.createQuery(query).getResultList();
    }

    public Todo find(Long id) {
        return entityManager.find(Todo.class, id);
    }

    public void remove(Long id) {
        Todo todo = entityManager.getReference(Todo.class, id);
        entityManager.remove(todo);
    }
}
