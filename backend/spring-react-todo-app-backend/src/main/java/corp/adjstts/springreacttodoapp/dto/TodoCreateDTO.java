package corp.adjstts.springreacttodoapp.dto;

public class TodoCreateDTO {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
