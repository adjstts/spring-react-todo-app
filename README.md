#Spring React Todo App
Todo list на Spring и React.

#API
## Методы

### Получение списка всех Todo
TYPE: GET  
URL: /api/todo

### Создание Todo
TYPE: POST  
URL: /api/todo  
PARAMS: `<TODO object without ID and CREATED>`

### Обновление Todo
TYPE: PUT  
URL: /api/todo/`id`  
PARAMS: `<TODO object without ID and CREATED>`

### Удаление Todo
TYPE: DELETE  
URL: /api/todo/`id`

## Объекты

```
TODO: {  
    id: <number>,  
    title: <string>,  
    created: <datetime>  
}
```
